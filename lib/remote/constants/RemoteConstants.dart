
class CoreRemoteConstants {

  static String baseUrl = '';

}

class RequestConstants {
   static int pageLimit = 20;
}

class CoreHTTPMethod {
  static const String post = 'POST';
  static const String get = 'GET';
  static const String put = 'PUT';
  static const String delete = 'DELETE';
  static const String patch = 'PATCH';
}